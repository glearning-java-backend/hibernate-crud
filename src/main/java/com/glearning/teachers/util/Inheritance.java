package com.glearning.teachers.util;

class Doctor {

	public void treatPatient() {
		System.out.println("Treating patients");
	}

}

class Orthopedician extends Doctor {

	@Override
	public void treatPatient() {
		System.out.println("Treating Ortho patients");
	}
}

public class Inheritance {
	public static void main(String[] args) {
		
		Doctor ortho = new Orthopedician();
		ortho.treatPatient();

	}
}
