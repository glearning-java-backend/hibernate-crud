package com.glearning.teachers.util;

import java.util.Properties;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import com.mysql.cj.xdevapi.SessionFactory;

public class HibernateUtil {
	
	private static SessionFactory sessionFactory;
	
	public static SessionFactory getSessionFactory() {
		
		Configuration configuration = new Configuration();
		
		Properties hiberateProperties = new Properties();
		hiberateProperties.put(Environment.DRIVER, "");
		hiberateProperties.put(Environment.URL, "");
		hiberateProperties.put(Environment.USER, "");
		hiberateProperties.put(Environment.PASS, "");
		hiberateProperties.put(Environment.DIALECT, "");
		
		configuration.setProperties(hiberateProperties);
		return null;
	}

}
